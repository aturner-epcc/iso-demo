[Back to CSE Team ISO process page...](cse_team.md)

Process Owner
=============

| Process owner | Quality team member |
|---------------|---------------------|
| Andy Turner   | Neelofer Banglawala |
||

Process Description
===================

Process for installing or updating software package on ARCHER by the CSE team.

Objectives
==========

-   Install/update CSE managed software package on ARCHER as, for example, requested through In Depth queries in line with the Quality Objective to provide a quality service which makes our customers happy and meets their needs.

Scope
=====

Any staff member managing centrally-installed CSE software

Inputs
======

-   Software package installation or update request passed to CSE team.
    -   Can be from Helpdesk first line support
    -   Can be internal or request direct to CSE team, raise query
    -   Can be external request direct to CSE team, raise query

Procedure/Work Description
==========================

1.  If it does not already exist, an In Depth query should be created to capture package installation/update request.
2.  Proceed according to [In Depth Query Handling](In_Depth_Query_Handling "wikilink") process
3.  If this will impact an already existing installation then:
    1.  Can the change be made without impacting current users? If so, use the [Requests For Change (RFC)](Requests_For_Change_(RFC) "wikilink") Agreed Change method
    2.  If not, then other users must be informed with at least 1 weeks notice of the planned change and an agreed change added as an RFC with clear remediation strategy at: [Requests For Change (RFC)](Requests_For_Change_(RFC) "wikilink")
4.  Any new or updated build instructions for packages should be added to the GitHub repo: <https://github.com/ARCHER-CSE/build-instructions>
5.  If a module is created as part of the installation/update then the CSE team member should add their name to it in the help function along with the date of installation/update.

Outputs
=======

-   New/updated software installation on ARCHER
-   Resolved query
-   (Possibly) feedback on support received from CSE team

Resources
=========

-   CSE team member to handle request
-   Documented procedure/work description for packages and modules: Main\_Page\#Packages\_and\_Modules
-   Documented procedure/work description from [In Depth Query Handling](In_Depth_Query_Handling "wikilink") process

Controls
========

-   CSE team member should understand role and package installation/update process
-   All completed resolved requests should ask for feedback on handling when closed
-   Software package installation/update procedure documents should be up-to-date

Criteria
========

-   Captured in [In Depth Query Handling](In_Depth_Query_Handling "wikilink") process

Measures/Monitoring
===================

-   Captured in [In Depth Query Handling](In_Depth_Query_Handling "wikilink") process

Risks
=====

Risks from Captured in [In Depth Query Handling](In_Depth_Query_Handling "wikilink") process process apply.

Additional risks:

| ID  | Risk                                                            | Impact | Likelihood | Mitigation                                                                                                          |
|-----|-----------------------------------------------------------------|--------|------------|---------------------------------------------------------------------------------------------------------------------|
| 1   | In Depth query not raised to handle installation/update request | High   | Low        | Monitor installations updates and check for associated queries. Undertake training of CSE team members if required. |
| 2   | Change adversely impacts users                                  | High   | Low        | Changes that may impact users captured in RFC process and remediation strategy identified.                          |

Version Control
===============

| Version | Date       | Change made                                         | Change made by                   | Approval date | Approved by | Comments                                                                                    |
|---------|------------|-----------------------------------------------------|----------------------------------|---------------|-------------|---------------------------------------------------------------------------------------------|
| 0.1     | 2016-08-29 | Initial Process Document                            | Andy Turner                      | 2016-08-29    | Andy Turner | None                                                                                        |
| 1.0     | 2016-10-25 | Reviewed and finalised                              | Andy Turner, Neelofer Banglawala | 2016-10-25    | Andy Turner | None                                                                                        |
| 1.1     | 2017-02-16 | Added reference to Quality Objectives to Objectives | Neelofer Banglawala              | 2017-02-17    | Andy Turner | Updated process due to Redmine finding 370: <https://safe-redmine.epcc.ed.ac.uk/issues/370> |
| 1.2     | 2017-02-20 | Added note on build instructions repo               | Andy Turner                      |               |             | Reviewed on 2017-02-20 by Neelofer Banglawala                                               |


